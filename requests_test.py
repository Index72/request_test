import pandas as pd
import pytest
import json
import re
import logging
import allure

import requests


class RequestData:
    def __init__(self) -> None:
        # 测试ID
        self.TestId:str=""
        # 测试标题
        self.Title:str = ""
        # 测试描述
        self.Description: str = ""
        # 测试点
        self.TestSectionDesc: str = ""
        self.URL: str = ""
        self.Method: str = ""
        self.Headers: dict = None  
        self.Parameters: dict = None
        self.Body: dict = None
        self.ExpectedStatusCode: int = 200  
        self.ExpectedResponse: dict = None
        self.result_verify_code:str =""
    def run_verify_code(self,response:requests.Response):
        """执行结果验证的代码"""
        exec(self.result_verify_code)
        pass


def to_RequestData(df: pd.DataFrame):
    df=df.astype("string")
    df=df.fillna("")
    out_list = []
    for i in range(df.shape[0]):
        data = RequestData()
        data.URL = df.iloc[i].get("URL", "").replace("\n","")
        data.Method = df.iloc[i].get("Method", "")
        h=str(df.iloc[i].get("Headers", ""))
        headers={}
        for line in h.splitlines():
            try:
                key=line[:line.find(":")].replace(" ","")
                value=line[line.find(":")+1:]
                
                if value[0] == " ":
                    value=value[1:]
                #print(f"key:{key} value:{value}")
                #print(key)
                headers[key]=value
            except:pass
        #print(headers)

        data.Headers = headers
        data.Parameters = df.iloc[i].get("Parameters", "")
        
        data.Body = df.iloc[i].get("Body","")
        data.Description=df.iloc[i].get("描述","")
        data.TestId=df.iloc[i].get("测试ID","")
        
        data.TestSectionDesc=df.iloc[i].get("测试点","")

        data.ExpectedStatusCode = df.iloc[i].get("ExpectedStatusCode", "")
        data.ExpectedResponse = df.iloc[i].get("ExpectedResponse", "")
        data.result_verify_code = df.iloc[i].get("结果验证", "")
        out_list.append(data)
    return out_list



def send_request(request_data: RequestData):
    # 发送请求

    #response = requests.request(method=request_data.Method or "",
    #                            url=request_data.URL or "",
    #                            headers=request_data.Headers or "",
    #                            params=request_data.Parameters or "",
    #                            json=request_data.Body or "")
    
    url = request_data.URL+"?"+request_data.Parameters

    payload = request_data.Body
    headers=request_data.Headers
    response = requests.request(request_data.Method, url, headers=headers, data=payload)
    
    # 返回响应体，其实际内容取决于请求
    return response




@pytest.mark.parametrize("test", to_RequestData(pd.DataFrame(pd.read_excel("./接口请求.xlsx"))))
def test_request(test: RequestData):
    #print(test.Headers.get("User-Agent"))
    #print(test)
    allure.dynamic.link(test.URL)
    allure.dynamic.title(test.Title)
    allure.dynamic.description(test.Description)
    
    #allure.dynamic.parameter("aa","cc")
    logging.info(f"request_url: {test.URL}")
    response=send_request(test)
    test.run_verify_code(response)

