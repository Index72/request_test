#!/bin/bash
source ~/.bashrc
pytest requests_test.py -vv -s --alluredir="./allure_report"

allure generate --clean ./allure_report

if [ -d /opt/allure-report ];
then
    rm -rf /opt/allure-report
fi
mv allure-report /opt/

